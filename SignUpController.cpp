// SignUpController.cpp : implementation file
//

#include "pch.h"
#include "HabiticaBun.h"
#include "SignUpController.h"
#include "afxdialogex.h"
#include "string"
#include "RepoUser.h"
#include "ControllerUser.h"
#include "HabiticaBunDlg.h"
using namespace std;

// SignUpController dialog

IMPLEMENT_DYNAMIC(SignUpController, CDialogEx)

SignUpController::SignUpController(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG2, pParent)
{

}

SignUpController::~SignUpController()
{
}

void SignUpController::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, usernameVariable);
	DDX_Control(pDX, IDC_EDIT2, passwordVariable);
	DDX_Control(pDX, IDC_EDIT3, emailVariable);
}


BEGIN_MESSAGE_MAP(SignUpController, CDialogEx)
	ON_BN_CLICKED(IDOK, &SignUpController::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &SignUpController::OnBnClickedButton1)
END_MESSAGE_MAP()


// SignUpController message handlers


void SignUpController::OnBnClickedOk()
{
	CString strText, strLine;
	int i, nLineCount = emailVariable.GetLineCount();
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = emailVariable.LineLength(emailVariable.LineIndex(i));
		emailVariable.GetLine(i, strText.GetBuffer(len), len);
		//strText.ReleaseBuffer(len);
		//strLine.Format(_T("line %d: '%s'\n"), i, strText);
		//AFXDUMP(strLine);
	}
	string email(CW2A(strText.GetString()));

	nLineCount = passwordVariable.GetLineCount();
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = passwordVariable.LineLength(passwordVariable.LineIndex(i));
		passwordVariable.GetLine(i, strText.GetBuffer(len), len);
		//strText.ReleaseBuffer(len);
		//strLine.Format(_T("line %d: '%s'\n"), i, strText);
		//AFXDUMP(strLine);
	}
	string password(CW2A(strText.GetString()));

	nLineCount = usernameVariable.GetLineCount();
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = usernameVariable.LineLength(usernameVariable.LineIndex(i));
		usernameVariable.GetLine(i, strText.GetBuffer(len), len);
		//strText.ReleaseBuffer(len);
		//strLine.Format(_T("line %d: '%s'\n"), i, strText);
		//AFXDUMP(strLine);
	}
	string username(CW2A(strText.GetString()));
	
	RepoUser repoUser;
	ControllerUser controllerUser(repoUser);
	controllerUser.addUser(email,password, username);

	CHabiticaBunDlg hab = new CHabiticaBunDlg;
	hab.DoModal();
}


void SignUpController::OnBnClickedButton1()
{
	this->EndDialog(0);
}
