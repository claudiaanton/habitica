
// HabiticaBunDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "HabiticaBun.h"
#include "HabiticaBunDlg.h"
#include "afxdialogex.h"
#include "MainController.h"
#include "SignUpController.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CHabiticaBunDlg dialog



CHabiticaBunDlg::CHabiticaBunDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_HABITICABUN_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHabiticaBunDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EMAILTEXT, emailVariable);
	DDX_Control(pDX, IDC_PASSWORD, passwordVariable);
}

void CHabiticaBunDlg::init(ControllerUser* contr, TemplateActivityList* temp)
{
	this->contr = contr;
	this->temp = temp;
}
	

BEGIN_MESSAGE_MAP(CHabiticaBunDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDRETRY, &CHabiticaBunDlg::OnBnClickedRetry)
	ON_EN_CHANGE(IDC_EMAILTEXT, &CHabiticaBunDlg::OnEnChangeEmailtext)
	ON_BN_CLICKED(IDC_CLOSE, &CHabiticaBunDlg::OnBnClickedClose)
END_MESSAGE_MAP()


// CHabiticaBunDlg message handlers

BOOL CHabiticaBunDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CHabiticaBunDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHabiticaBunDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHabiticaBunDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//Log in 
void CHabiticaBunDlg::OnBnClickedRetry()
{
	
	CString strText,strLine;
	//GetDlgItemText(emailTextField,st);
	int i,nLineCount = emailVariable.GetLineCount();
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = emailVariable.LineLength(emailVariable.LineIndex(i));
		emailVariable.GetLine(i, strText.GetBuffer(len), len);
		strText.ReleaseBuffer(len);
		strLine.Format(_T("line %d: '%s'\n"), i, strText);
		AFXDUMP(strLine);
	}
	
	//RepoUser repoUser;
	// controllerUser(repoUser);
	contr->addUser("a@yahoo.com", "a", "ana");

	std::string username(CW2A(strText.GetString()));
	User user = contr->findOne(username);
	if (user.getUsername() == "null")
	{
		string mesaj("Invalid email!");
		CString cstr(mesaj.c_str());
		AfxMessageBox(cstr);
	}

	nLineCount = passwordVariable.GetLineCount();
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = passwordVariable.LineLength(passwordVariable.LineIndex(i));
		passwordVariable.GetLine(i, strText.GetBuffer(len), len);
		strText.ReleaseBuffer(len);
		strLine.Format(_T("line %d: '%s'\n"), i, strText);
		AFXDUMP(strLine);
	}
	
	std::string password(CW2A(strText.GetString()));
	if (user.getPassword() == password) {
		MainController mainB = new MainController;
		mainB.init(contr,temp,username);
		mainB.DoModal();
	}
	else
	{
		string mesaj("Wrong password");
		CString cstr(mesaj.c_str());
		AfxMessageBox(cstr);
	}

	//AfxMessageBox(strLine);
	//AfxMessageBox(strText);

	
}


void CHabiticaBunDlg::OnEnChangeEmailtext()
{
	
}

void CHabiticaBunDlg::OnBnClickedClose()
{
	// TODO: Add your control notification handler code here
	//this->EndDialog(0);
	SignUpController sig = new SignUpController;
	sig.DoModal();
}
