//
// Created by Anton Claudia on 12.12.2021.
//

#ifndef HABITICA_CONTROLLERUSER_H
#define HABITICA_CONTROLLERUSER_H

#pragma once
#include<string>
#include "RepoUser.h"

using namespace std;

class ContrError
{
private:
    string err;
public:
    ContrError(const string& s) :err{ s } {};
    string get_error()
    {
        return err;
    }
};

class ControllerUser
{
private:
    RepoUser& repo;

public:
    ControllerUser(RepoUser& repo) : repo(repo) {}

    ///USER
    void addUser(const string& email, const string& password, const string& username);
    vector<User>& getAll() { return repo.getall(); }
    User findOne(const string& username);
    User& findOneRef(const string& username);

    ///HABITS

    ///DAILY
    void addDailyDuty(const string& usr, const string& st, const int& scor);
    void addDoneDuty(const string& usr, const string& st);
    vector<Daily> getAllDoneOfAUser(const string& st);
    //vector<string> getInProgressOfAUser(const string& st);





};

#endif //HABITICA_CONTROLLERUSER_H
