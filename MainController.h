#pragma once
#include <string>
#include "ControllerUser.h"
#include "TemplateActivityList.h"
// MainController dialog

 class MainController : public CDialogEx
{
	DECLARE_DYNAMIC(MainController)

public:
	MainController(CWnd* pParent = nullptr);   // standard constructor
	virtual ~MainController();
	void init(ControllerUser* contr, TemplateActivityList* temp, const string email);
private:
	ControllerUser* contr;
	TemplateActivityList* temp;
	string user;
	void addCategories();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLbnSelchangeActivity();
	afx_msg void OnLbnSelchangeMylist();
private:
	CListBox categoryVariable;
	CListBox activityVariable;
	CListBox myListVariable;
public:
	afx_msg void OnLbnSelchangeListcategory();
	afx_msg void OnBnClickedAddactivity();
private:
	CProgressCtrl progresBar;
public:
	afx_msg void OnNMCustomdrawProgress1(NMHDR* pNMHDR, LRESULT* pResult);
	CButton scoreVariable;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
 };
