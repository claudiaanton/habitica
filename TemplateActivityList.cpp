#include "pch.h"
#include "TemplateActivityList.h"

void TemplateActivityList::addCategory(const string nume, const int scor)
{
	Category cat(nume, scor);
	vec.push_back(cat);
}

void TemplateActivityList::addCategory(const string nume, const int scor, vector<Activity> activ)
{
	Category cat(nume, scor,activ);
	vec.push_back(cat);
}

Category& TemplateActivityList::findCategory(const string nume)
{
	for (Category& i : getAllCategories())
		if (i.get_name() == nume)
			return i;
}

