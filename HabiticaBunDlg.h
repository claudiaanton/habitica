
// HabiticaBunDlg.h : header file
//

#pragma once
#include "RepoUser.h"
#include "ControllerUser.h"
#include "TemplateActivityList.h"

// CHabiticaBunDlg dialog
class CHabiticaBunDlg : public CDialogEx
{
// Construction
public:
	CHabiticaBunDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_HABITICABUN_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
private:
	//RepoUser repoUser;
	ControllerUser* contr;
	TemplateActivityList* temp;
public:
	void init(ControllerUser* contr, TemplateActivityList* temp);
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedRetry();
	// email variable
	CEdit emailVariable;
	afx_msg void OnEnChangeEmailtext();
	afx_msg void OnEnChangeEdit2();
	// password variable
	CEdit passwordVariable;
	afx_msg void OnBnClickedClose();
};
