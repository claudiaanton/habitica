#pragma once
#include "TemplateActivityList.h"

// NewActivityController dialog

class NewActivityController : public CDialogEx
{
	DECLARE_DYNAMIC(NewActivityController)
private:
	TemplateActivityList* temp;

public:
	NewActivityController(CWnd* pParent = nullptr);   // standard constructor
	virtual ~NewActivityController();
	void init(TemplateActivityList* temp);
// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG3 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnCbnSelchangeCombo1();
	CEdit nameActivityVarible;
	CComboBox categoryVariableBox;
};
