#pragma once
#include "pch.h"
#include <pqxx/pqxx>
#include "ControllerUser.h"

void ControllerUser::addUser(const string& email, const string& password, const string& username)
{
    if (email.empty()) throw ContrError{ "Invalid email" };
    if (email.find('@.') == string::npos) throw ContrError{ "Invalid email" };
    if (username.empty()) throw ContrError{ "Invalid username" };
    for (const auto& i : getAll())
        if (i.getUsername() == username) throw ContrError{ "This username already exists" };
    
    repo.add(User{ username,email,password});
}



User ControllerUser::findOne(const string& username) {
    if (username.empty()) throw ContrError{ "Innvalid email" };
    for (User& i : getAll())
        if (i.getEmail() == username) return i;
    
    return User{ "null","",""};
    
}

User& ControllerUser::findOneRef(const string& username)
{
    for (User& i : getAll())
        if (i.getEmail() == username) return i;
}


