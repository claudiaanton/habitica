#pragma once


// SignUpController dialog

class SignUpController : public CDialogEx
{
	DECLARE_DYNAMIC(SignUpController)

public:
	SignUpController(CWnd* pParent = nullptr);   // standard constructor
	virtual ~SignUpController();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CEdit usernameVariable;
	CEdit passwordVariable;
	CEdit emailVariable;
	afx_msg void OnBnClickedButton1();
};
