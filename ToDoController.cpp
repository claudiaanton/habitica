// ToDoController.cpp : implementation file
//
#pragma once
#include "pch.h"
#include "HabiticaBun.h"
#include "ToDoController.h"
#include "afxdialogex.h"
#include <pqxx/pqxx>


// ToDoController dialog

IMPLEMENT_DYNAMIC(ToDoController, CDialogEx)

ToDoController::ToDoController(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG4, pParent)
{

}

ToDoController::~ToDoController()
{
}

void ToDoController::init(ControllerUser* contr, const string email)
{
	this->contr=contr;
	user = email;
}

void ToDoController::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT2, activityNameVariable);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, datePickerVariable);
	DDX_Control(pDX, IDC_LIST1, todoListVariable);
}

BEGIN_MESSAGE_MAP(ToDoController, CDialogEx)
	ON_BN_CLICKED(IDOK, &ToDoController::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &ToDoController::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &ToDoController::OnBnClickedButton2)
END_MESSAGE_MAP()

BOOL ToDoController::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	//SetIcon(m_hIcon, TRUE);			// Set big icon
	//SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	//User usr = contr->findOne();
	for (Activity i : contr->findOne(user).getToDo())
	{
		CTime ti = i.get_date();
		CString st;
		st.Format(_T("%d/%d/%d"), ti.GetDay(), ti.GetMonth(), ti.GetYear());
		todoListVariable.AddString(CString(i.get_name().c_str()) + " on " + st);
	}
	return TRUE;
}





// ToDoController message handlers


void ToDoController::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}


void ToDoController::OnBnClickedButton1()
{
	int i, nLineCount = activityNameVariable.GetLineCount();
	CString strText, strLine;
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = activityNameVariable.LineLength(activityNameVariable.LineIndex(i));
		activityNameVariable.GetLine(i, strText.GetBuffer(len), len);
		strText.ReleaseBuffer(len);
		strLine.Format(_T("line %d: '%s'\n"), i, strText);
		AFXDUMP(strLine);
	}
	string aname(CW2A(strText.GetString()));

	CTime ti;
	datePickerVariable.GetTime(ti);
	//date.Format("%d/%d/%d",yourvariable.GetDay(),yourvariable.GetMonth(),yourvariable.GetYear());
	//usr->addToDo(Activity(aname, gmtime(ti)));
	CString st;
	st.Format(_T("%d/%d/%d"), ti.GetDay(),ti.GetMonth(),ti.GetYear());
	if (!strText.IsEmpty())
	{
		string name(CW2A(strText.GetString()));
		contr->findOneRef(user).addToDo(Activity(name, ti));
		todoListVariable.AddString(strText + " on " + st);
	}
}

pqxx::result query()
{
	//postgresql:///mydb?host=localhost&port=5433

	//pqxx:connection c{ "dbname = habitica user = postgres password = postgres \
     localhost port = 5432" };
	//std: :string connectionString="host=localhost port=5432 dbname=test user = postgres password = 123454321";
	//pqxx::connection c{ "postgresql://postgres:postgres@localhost&port=5432/habitica" };
	
	string st("host=localhost port=5432 dbname=habitica user = postgres password = postgres");
	pqxx::connection c(st.c_str());
	pqxx::work txn{ c };

	pqxx::result r{ txn.exec("SELECT nume FROM persoane") };
	for (auto row : r)
		std::cout
		// Address column by name.  Use c_str() to get C-style string.
		<< row["nume"].c_str();
		//<< " makes "
		// Address column by zero-based index.  Use as<int>() to parse as int.
		//<< row[1].as<int>()
		//<< "."
		//<< std::endl;

	// Not really needed, since we made no changes, but good habit to be
	// explicit about when the transaction is done.
	txn.commit();

	// Connection object goes out of scope here.  It closes automatically.
	return r;
}


void ToDoController::OnBnClickedButton2()
{
	/*
	// TODO: Add your control notification handler code here
	try
	{
		pqxx::result r{ query() };

		// Results can be accessed and iterated again.  Even after the connection
		// has been closed.
		for (auto row : r)
		{
		
			// Iterate over fields in a row.
			for (auto field : row)
			{
				
				todoListVariable.AddString(CString(field.c_str()));
			}
			
		}
	}
	catch (pqxx::sql_error const& e)
	{
		std::cerr << "SQL error: " << e.what() << std::endl;
		std::cerr << "Query was: " << e.query() << std::endl;
	}
	catch (std::exception const& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}
	*/
	int n = todoListVariable.GetCaretIndex();
	todoListVariable.DeleteString(n);
}
