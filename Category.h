#pragma once
#include<iostream>
#include<vector>
#include "Activity.h"
using namespace std;

class Category {
    string nume;
    int scor;
    vector<Activity> subcategory;

public:
    Category(const string nume, const int scor);
    Category(const string nume, const int scor, vector<Activity> acviv);
    void addActivity(const string act);
    vector<Activity> getActivities();
    const string get_name();
    int get_scor();
    
};