#pragma once


// HabitsController dialog

class HabitsController : public CDialogEx
{
	DECLARE_DYNAMIC(HabitsController)

public:
	HabitsController(CWnd* pParent = nullptr);   // standard constructor
	virtual ~HabitsController();
	BOOL HabitsController::OnInitDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG5 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox goodVariable;
	CListBox badVariable;
};
