// HabitsController.cpp : implementation file
//
#pragma once
#include "pch.h"
#include <pqxx/pqxx>
#include "HabiticaBun.h"
#include "HabitsController.h"
#include "afxdialogex.h"


// HabitsController dialog

IMPLEMENT_DYNAMIC(HabitsController, CDialogEx)

HabitsController::HabitsController(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG5, pParent)
{

}

HabitsController::~HabitsController()
{
}

void HabitsController::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, goodVariable);
	DDX_Control(pDX, IDC_LIST2, badVariable);
}

BOOL HabitsController::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	//SetIcon(m_hIcon, TRUE);			// Set big icon
	//SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	goodVariable.AddString(CString("Worked out"));
	goodVariable.AddString(CString("Meditate"));

	badVariable.AddString(CString("Smoking"));
	return TRUE;  // return TRUE  unless you set the focus to a control
}

pqxx::result query1()
{
	pqxx::connection c{ "postgresql://postgres:postgres@localhost&port=5432/habitica" };
	pqxx::work txn{ c };

	pqxx::result r{ txn.exec("SELECT nume,tip,COUNT(nume)"
							 "FROM Activity"
							 "WHERE DATEDIFF(day, date, GETDATE()) = 7"
							 "GROUP BY nume"
							 "HAVING COUNT(nume)>2 ") };

	txn.commit();

	return r;
}


BEGIN_MESSAGE_MAP(HabitsController, CDialogEx)
END_MESSAGE_MAP()


// HabitsController message handlers
