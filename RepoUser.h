//
// Created by Anton Claudia on 12.12.2021.
//

#ifndef HABITICA_REPOUSER_H
#define HABITICA_REPOUSER_H

#pragma once
#include<string>
#include <vector>
#include "User.h"

using namespace std;

class RepoUser
{
private:
    vector<User> users;
public:
    void add(const User& a);
    vector<User>& getall();
    //void addToDo(const User& u, const ToDoList& todo);
    void addDaily(const User& u, const Activity& ac);
};



#endif //HABITICA_REPOUSER_H
