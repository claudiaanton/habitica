// NewActivityController.cpp : implementation file
//

#include "pch.h"
#include "HabiticaBun.h"
#include "NewActivityController.h"
#include "afxdialogex.h"


// NewActivityController dialog

IMPLEMENT_DYNAMIC(NewActivityController, CDialogEx)

NewActivityController::NewActivityController(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG3, pParent)
{
}

NewActivityController::~NewActivityController()
{
}

void NewActivityController::init(TemplateActivityList* temp)
{
	this->temp = temp;
}

void NewActivityController::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, nameActivityVarible);
	DDX_Control(pDX, IDC_COMBO1, categoryVariableBox);
}

BOOL NewActivityController::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	// Set the icon for this dialog.  The framework does this automatically
//  when the application's main window is not a dialog
//SetIcon(m_hIcon, TRUE);			// Set big icon
//SetIcon(m_hIcon, FALSE);		// Set small icon

// TODO: Add extra initialization here
	
	for (Category i : temp->getAllCategories())
	{
		CString cstr(i.get_name().c_str());
		categoryVariableBox.AddString(cstr);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}


BEGIN_MESSAGE_MAP(NewActivityController, CDialogEx)
	ON_BN_CLICKED(IDOK, &NewActivityController::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO1, &NewActivityController::OnCbnSelchangeCombo1)
END_MESSAGE_MAP()


// NewActivityController message handlers


void NewActivityController::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CDialogEx::OnOK();
	short cIndex;
	cIndex = categoryVariableBox.GetCurSel();
	CString fontName;
	categoryVariableBox.GetLBText(cIndex, fontName);
	string name(CW2A(fontName.GetString()));
	//Category cat=temp->findCategory(name);

	int i, nLineCount = nameActivityVarible.GetLineCount();
	CString strText, strLine;
	for (i = 0; i < nLineCount; i++)
	{
		// length of line i:
		int len = nameActivityVarible.LineLength(nameActivityVarible.LineIndex(i));
		nameActivityVarible.GetLine(i, strText.GetBuffer(len), len);
		strText.ReleaseBuffer(len);
		strLine.Format(_T("line %d: '%s'\n"), i, strText);
		AFXDUMP(strLine);
	}
	string aname(CW2A(strText.GetString()));
	temp->findCategory(name).addActivity(aname);

	this->EndDialog(0);
}


void NewActivityController::OnCbnSelchangeCombo1()
{
	// TODO: Add your control notification handler code here
}
