//
// Created by Anton Claudia on 06.12.2021.
//

#ifndef TRIEDIT_USER_H
#define TRIEDIT_USER_H

#pragma once

#include<string>
#include "Daily.h"
#include "ToDoList.h"

using namespace std;

class User {
private:
    string username, email, password;
    int  level, score;
    Daily daily;
    //    vector<Habits> habits;
        ToDoList todo;
    //    Daily &daily;
public:
    User(const string& username, const string& email, const string& password) : username(username), email(email),
        password(password) {}

    const string& getEmail() const { return email; }

    const string& getPassword() const { return password; }

    const string& getUsername() const { return username; }

    void setUsername(const string& username) { User::username = username; }

    void setScore(int s) { score = s; }

    void addScore(int s) { score += s; }

    int getScore() const { return score; }

    Daily getDailies() { return daily; }

    void addDailyActivity(const Activity& ac) { daily.addActivity(ac); }

    void addToDo(const Activity t) { todo.addActivity(t); }

    vector<Activity> getToDo() { return todo.getActivities();}

};

#endif //TRIEDIT_USER_H
