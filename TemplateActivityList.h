#pragma once
#include <vector>
#include "Category.h"
using namespace std;

class TemplateActivityList {
private:
    vector<Category> vec;
public:
    void addCategory(const string nume, const int scor);
    void addCategory(const string nume, const int scor,vector<Activity> activ);
    vector<Category>& getAllCategories() { return vec; }
    Category& findCategory(const string nume);
   
};
