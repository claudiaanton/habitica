#include "pch.h"
#include "RepoUser.h"

void RepoUser::add(const User& a)
{
	users.push_back(a);
}

vector<User>& RepoUser::getall()
{
	return users;
}

void RepoUser::addDaily(const User& usr, const Activity& ac)
{
	for (User& u : getall()) {
		if (u.getUsername() == usr.getUsername())
			u.addDailyActivity(ac);
	}
}
