// MainController.cpp : implementation file
//

#include "pch.h"
#include "HabiticaBun.h"
#include "MainController.h"
#include "afxdialogex.h"
#include "NewActivityController.h"
#include "ToDoController.h"
#include "HabitsController.h"
#include <string>
#include<iostream>
#include <atlstr.h>
#pragma warning(disable : 4996)
using namespace std;

// MainController dialog


IMPLEMENT_DYNAMIC(MainController, CDialogEx)

MainController::MainController(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{
}

void MainController::init(ControllerUser* contr, TemplateActivityList* temp, const string email)
{
	this->contr = contr;
	this->temp = temp;
	user = email;
	progresBar.SetRange(0, 100);
	progresBar.SetStep(5/10);
}

void MainController::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_LISTCATEGORY, categoryVariable);
	DDX_Control(pDX, IDC_ACTIVITY, activityVariable);
	DDX_Control(pDX, IDC_MYLIST, myListVariable);
	DDX_Control(pDX, IDC_PROGRESS1, progresBar);
	//DDX_Control(pDX, IDCANCEL, scoreVariable);
	DDX_Control(pDX, IDC_LISTCATEGORY, categoryVariable);
}


BEGIN_MESSAGE_MAP(MainController, CDialogEx)
	ON_LBN_SELCHANGE(IDC_ACTIVITY, &MainController::OnLbnSelchangeActivity)
	ON_LBN_SELCHANGE(IDC_MYLIST, &MainController::OnLbnSelchangeMylist)
	ON_LBN_SELCHANGE(IDC_LISTCATEGORY, &MainController::OnLbnSelchangeListcategory)
	ON_BN_CLICKED(IDC_ADDACTIVITY, &MainController::OnBnClickedAddactivity)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PROGRESS1, &MainController::OnNMCustomdrawProgress1)
	ON_BN_CLICKED(IDC_BUTTON1, &MainController::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &MainController::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &MainController::OnBnClickedButton3)
END_MESSAGE_MAP()

BOOL MainController::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	//SetIcon(m_hIcon, TRUE);			// Set big icon
	//SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	
	for (Category i : temp->getAllCategories())
		categoryVariable.AddString(CString (i.get_name().c_str()));

	return TRUE;  // return TRUE  unless you set the focus to a control
}


void MainController::addCategories()
{
	
}

MainController::~MainController()
{

}

// MainController message handlers



void MainController::OnLbnSelchangeActivity()
{
	// TODO: Add your control notification handler code here
}


void MainController::OnLbnSelchangeMylist()
{
	// TODO: Add your control notification handler code here
}



void MainController::OnLbnSelchangeListcategory()
{
	// TODO: Add your control notification handler code here
	activityVariable.ResetContent();
	int nSel = categoryVariable.GetCurSel();
	CString ItemSelected;
	if (nSel != LB_ERR)
	{
		categoryVariable.GetText(nSel, ItemSelected);
		//AfxMessageBox(ItemSelected);
	}
	//std::string selected(CW2A(ItemSelected.GetString()));
	string name(CW2A(ItemSelected.GetString()));
	Category cat = temp->findCategory(name);
	for (Activity i : cat.getActivities())
	{
		CString cstr(i.get_name().c_str());
		activityVariable.AddString(cstr);
	}
}


void MainController::OnBnClickedAddactivity()
{
	// TODO: Add your control notification handler code here
	int nSel=activityVariable.GetCurSel();
	CString ItemSelected;
	if (nSel != LB_ERR)
	{
		activityVariable.GetText(nSel, ItemSelected);
		myListVariable.AddString(ItemSelected);
		
		User usr =contr->findOne(user);
		time_t t = std::time(0);   
		tm* now = std::localtime(&t);
		string name(CW2A(ItemSelected.GetString()));
		Activity act(name);
		usr.addDailyActivity(act);

		//TemplateActivityList tep;
		int nSelc = categoryVariable.GetCurSel();
		CString ItemSelectedc;
		if (nSelc != LB_ERR)
		{
			categoryVariable.GetText(nSelc, ItemSelectedc);
			//AfxMessageBox(ItemSelected);
		}
		string n(CW2A(ItemSelectedc.GetString()));
		Category cat = temp->findCategory(n);
		usr.addScore(cat.get_scor());
		//int a = myListVariable.GetCount();
		if (cat.get_name() == "Bad")
			progresBar.SetPos(progresBar.GetPos() / 2);
		else progresBar.StepIt();
		//GetDlgItem(IDC_STATIC).SetWindowText("some text");
		//scoreVariable.SetWindowText(TEXT("Score"+ usr.getScore()));
		//progresBar.SetPos(usr.getScore());
	}
}


void MainController::OnNMCustomdrawProgress1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void MainController::OnBnClickedButton1()
{
	NewActivityController act= new NewActivityController;
	act.init(temp);
	act.DoModal();
}


void MainController::OnBnClickedButton2()
{
	ToDoController to = new ToDoController;
	//contr->findOneRef(user).addToDo(Activity("okok"));
	//contr->findOneRef(user)->addToDo(Activity("dada"));
	to.init(contr,user);
	to.DoModal();
}


void MainController::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	HabitsController ct = new HabitsController;
	ct.DoModal();
}
