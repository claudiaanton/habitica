//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HabiticaBun.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_HABITICABUN_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     133
#define IDD_DIALOG3                     135
#define IDD_DIALOG4                     137
#define IDD_DIALOG5                     139
#define IDC_EDIT1                       1000
#define IDC_EMAILTEXT                   1000
#define IDC_EDIT2                       1001
#define IDC_PASSWORD                    1001
#define IDC_BUTTON1                     1003
#define IDC_BUTTON2                     1004
#define IDC_BUTTON3                     1005
#define IDC_PROGRESS1                   1006
#define IDC_ACTIVITY                    1008
#define IDC_ADDACTIVITY                 1012
#define IDC_MYLIST                      1014
#define IDC_CLOSE                       1015
#define IDC_LISTCATEGORY                1016
#define IDC_EDIT3                       1017
#define IDC_COMBO1                      1020
#define IDC_LIST1                       1021
#define IDC_DATETIMEPICKER1             1022
#define IDC_LIST2                       1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
