
// HabiticaBun.cpp : Defines the class behaviors for the application.
//
#pragma once
#include "pch.h"
#include "framework.h"
#include "ControllerUser.h"
#include "HabiticaBun.h"
#include "HabiticaBunDlg.h"
#include "TemplateActivityList.h"
#include "ToDoList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

RepoUser repo;
TemplateActivityList temp;
ControllerUser contr(repo);

// CHabiticaBunApp

BEGIN_MESSAGE_MAP(CHabiticaBunApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CHabiticaBunApp construction

CHabiticaBunApp::CHabiticaBunApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CHabiticaBunApp object

CHabiticaBunApp theApp;


// CHabiticaBunApp initialization

BOOL CHabiticaBunApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CHabiticaBunDlg dlg;
	m_pMainWnd = &dlg;
	
	vector<Activity> ac;
	ac.push_back(Activity("Swimming"));
	ac.push_back(Activity("Tennis"));
	ac.push_back(Activity("Badminton"));
	ac.push_back(Activity("Ping-Pong"));
	ac.push_back(Activity("Climbing"));
	temp.addCategory("Sport", 10, ac);

	ac.clear();
	ac.push_back(Activity("Finished tasks"));
	ac.push_back(Activity("Learned new skill"));
	temp.addCategory("Work", 5,ac);

	ac.clear();
	ac.push_back(Activity("Doing laundry"));
	ac.push_back(Activity("Vacuuming"));
	ac.push_back(Activity("Doing the dishes"));
	ac.push_back(Activity("Making the bed"));
	temp.addCategory("Chores", 5,ac);

	ac.clear();
	ac.push_back(Activity("Worked out"));
	ac.push_back(Activity("30min run"));
	ac.push_back(Activity("30min walk"));
	ac.push_back(Activity("Meditate"));
	temp.addCategory("Health", 10,ac);

	ac.clear();
	ac.push_back(Activity("Smoking"));
	ac.push_back(Activity("Drinking"));
	ac.push_back(Activity("Eating fast-food"));
	temp.addCategory("Bad", -10, ac);


	dlg.init(&contr, &temp);
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

