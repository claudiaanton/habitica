#include "pch.h"
#include "Category.h"

Category::Category(const string nume, const int scor) : nume(nume), scor(scor) {}

Category::Category(const string nume, const int scor, vector<Activity> activ) : nume(nume), scor(scor), subcategory(activ) {}

void Category::addActivity(const string act)
{
	subcategory.push_back(Activity(act));
}

vector<Activity> Category::getActivities()
{
	return subcategory;
}

const string Category::get_name()
{
	return nume;
}

int Category::get_scor()
{
	return scor;
}