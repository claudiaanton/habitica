#pragma once
#include<string>
#include <vector>
using namespace std;

class  Activity {
    string name;
    //tm* date;
    CTime date;
    
public:
    Activity(const string& st, CTime tm):name(st),date(tm){}
    Activity(const string& st) :name(st) {}
    const string get_name();
    CTime get_date() { return date; };
    void set_name(const string a);
    //void set_date(tm* d) { date = d; }
};
