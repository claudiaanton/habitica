#pragma once
#include "ControllerUser.h"

// ToDoController dialog

class ToDoController : public CDialogEx
{
	DECLARE_DYNAMIC(ToDoController)

public:
	ToDoController(CWnd* pParent = nullptr);   // standard constructor
	virtual ~ToDoController();
	void init(ControllerUser* contr, const string email);
private:
	ControllerUser* contr;
	string user;

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG4 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CEdit activityNameVariable;
	CDateTimeCtrl datePickerVariable;
	CListBox todoListVariable;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
