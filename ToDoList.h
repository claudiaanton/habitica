//
// Created by Anton Claudia on 06.12.2021.
//

#ifndef TRIEDIT_TODOLIST_H
#define TRIEDIT_TODOLIST_H

#pragma once
#include<string>
#include <vector>
#include <iostream>
#include "Activity.h"
using namespace std;

class ToDoList
{
private:
    vector<Activity> toDos;

public:

    ///Setter-ish
    void addActivity(const Activity st) { toDos.push_back(st); }

    ///Getters
    vector<Activity> getActivities() { return toDos; }
};

#endif //TRIEDIT_TODOLIST_H
